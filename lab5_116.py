#Oleksii Frankov, K-13.1, V-116.

from sys import argv
import json
from information import *
from loading import *

def _output_creator():
    print("The program was created by Oleksii Frankov, group K-13.1.")

def _output_task():
    print("""Variant-116. Find all students that have 100 points for any subject.
Output information about their studying results.""")

def _output_stars():
    print("*****")

def _output_error():
    print("***** program aborted *****")

def load_ini(path_ini):
    """This function loads information of ini file.

    If necessary key is absent, the Exception raises."""
    with open(path_ini, encoding='utf-8') as f:
        data_ini = json.load(f)
    _ini_check(data_ini)
    _ini_input_check(data_ini)
    _ini_output_check(data_ini)
    return data_ini["input"], data_ini["output"]

def _ini_check(data_ini):
    if "input" and "output" in (data_ini.keys()):
        pass
    else:
        raise Exception


def _ini_input_check(data_ini):
    if "csv" and "json" and "encoding" in (data_ini["input"].keys()):
        pass
    else:
        raise Exception

def _ini_output_check(data_ini):
    if "fname" and "encoding" in (data_ini["output"].keys()):
        pass
    else:
        raise Exception

def main(path_ini):
    print(f'ini {path_ini}:', end=" ")
    in_data, out_data = load_ini(path_ini)
    csv_name = in_data["csv"]
    json_name = in_data["json"]
    encoding_in = in_data["encoding"]
    fname_out = out_data["fname"]
    encoding_out = out_data["encoding"]
    print("OK")
    info = Information()
    with info:
        load(info, csv_name, json_name, encoding_in)
        print(f"output {fname_out}:", end=" ")
        info.output(fname_out, encoding_out)
        print("OK")

def process(argv):
    _output_creator()
    _output_task()
    _output_stars()
    if len(argv) != 2:
        _output_error()
    else:
        try:
            main(argv[1])
        except BaseException:
            print("UPS")
            _output_error()


process(argv)

