#Oleksii Frankov, K-13.1, V-116.

import re

class Information():
    """This class contains information about students and number of records.

    Each student is unique. If different students have the same number of pass, Exception raises. """
    def __init__(self):
        self._students = []
        self._num_records = 0
        self._tails = set()

    @property
    def num_records(self):
        return(self._num_records)

    @property
    def tails(self):
        return len(self._tails)

    def __iter__(self):
        return iter(self._students)

    def load(self, gov_point, name, patronymic, exam_point, surname,
             total_point, subname, ngroup, npass):
        """This method distributes data among objects of classes Student and Subject."""
        self._num_records+=1
        student = self.add_student(name, patronymic, surname,
        ngroup, npass)
        student.add_subject(subname, gov_point,
                 exam_point, total_point)
        self._stud_tail(student, gov_point)

    def _stud_tail(self, student, gov_point):
        if gov_point < 3:
            self._tails.add(student)

    def find_student(self, npass:str):
        for el in self:
            if el.npass == npass:
                return el
        return None

    def add_student(self, name: str, patronymic: str, surname: str,
                 ngroup: str, npass: str):
        student = self.find_student(npass)
        if student:
            student.student_check(name, patronymic, surname,
                 ngroup)
        else:
            student = Student(name, patronymic, surname,
                 ngroup, npass)
            self._students.append(student)
        return student

    def clear(self):
        """This method deletes whole data in object of Information."""
        self._students = []
        self._num_records = 0
        self._tails = set()

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_val:
            self.clear()



    def output(self, fname_out, encoding_out):
        """This method writes the necessary statictics in output file."""
        with open(fname_out, 'w', encoding=encoding_out) as f:
            for stud in self:
                if stud.progress:
                    stud._output(f)

class Student():
    """This class contains information about student."""
    _name_pat = re.compile(r'[\w -`]{,22}')
    _d_pat = re.compile(r'[\d_]')
    _pat = re.compile(r'[_]')
    _patronymic_pat = re.compile(r'[\w -`]{,25}')
    _surname_pat = re.compile(r'[\w -`]{,26}')
    _ngroup_pat = re.compile(r'[\w-]{1,4}')
    _npass_pat = re.compile(r'[\d]{6}')
    def __init__(self, name: str, patronymic: str, surname: str,
                 ngroup: str, npass: str):
        self._name_pat_check(name)
        self._name = name
        self._patronymic_pat_check(patronymic)
        self._patronymic = patronymic
        self._surname_pat_check(surname)
        self._surname = surname
        self._ngroup_pat_check(ngroup)
        self._ngroup = ngroup
        self._npass_pat_check(npass)
        self._npass = npass
        self._progress = False
        self._subjects = []
        self._num100 = 0
        self._sum_point = 0

    def _name_pat_check(self, name):
        res = re.fullmatch(Student._name_pat, name) and not re.search(Student._d_pat, name)
        if not res:
            raise Exception

    def _patronymic_pat_check(self, patronymic):
        res = re.fullmatch(Student._patronymic_pat, patronymic) and not re.search(Student._d_pat, patronymic)
        if not res:
            raise Exception

    def _surname_pat_check(self, surname):
        res = re.fullmatch(Student._surname_pat, surname) and not re.search(Student._d_pat, surname)
        if not res:
            raise Exception

    def _ngroup_pat_check(self, ngroup):
        res = re.fullmatch(Student._ngroup_pat, ngroup) and not re.search(Student._pat, ngroup)
        if not res:
            raise Exception

    def _npass_pat_check(self, npass):
        if not  re.fullmatch(Student._npass_pat, npass):
            raise Exception

    def student_check(self, name, patronymic, surname,
                 ngroup):
        """This method checks if data about student is correct.

        If data is wrong, Exception raises."""
        if self.name != name or self.patronymic != patronymic\
                or self.surname != surname or self.ngroup != ngroup:
            raise Exception

    @property
    def num100(self):
        return self._num100

    @property
    def patronymic(self):
        return self._patronymic

    @property
    def mean_point(self):
        return round(self._sum_point / len(self._subjects), 1)

    @property
    def name(self):
        return self._name

    @property
    def surname(self):
        return self._surname

    @property
    def npass(self):
        return self._npass

    @property
    def ngroup(self):
        return self._ngroup

    @property
    def progress(self):
        return self._progress

    def find_subject(self, subname):
        for el in self._subjects:
            if el.subname == subname:
                raise Exception

    def add_subject(self, subname: str, gov_point: int,
                 exam_point: int, total_point: int):
        self.find_subject(subname)
        subject = Subject(subname, gov_point,
                 exam_point, total_point)
        self._subjects.append(subject)
        self._subject_100_check(total_point)
        self._sum_point+=total_point

    def _subject_100_check(self, total_point):
        if total_point == 100:
            self._progress = True
            self._num100 +=1

    def __eq__(self, other):
        return self.npass == other.npass

    def __hash__(self):
        return hash(self.npass)

    def _output(self, f):
        f.write(f"{self.num100}")
        f.write('\t' + f"{self.npass}")
        f.write('\t' + f"{self.surname}")
        f.write('\t' + f"{self.name}")
        f.write('\t' + f"{self.mean_point}" + '\n')
        self._subjects.sort(reverse=True)
        for sub in self._subjects:
            if sub < 100:
                f.write('\t' + f"{sub.total_point}")
                f.write('\t' + f"{sub.subname}" + '\n')

class Subject():
    """This class contains information about a subject of the student."""
    _subname_pat = re.compile(r'[\w" -]{5,59}')
    def __init__(self, subname: str, gov_point: int,
                 exam_point: int, total_point: int):
        self._check(gov_point, exam_point, total_point)
        self._subname_pat_check(subname)
        self._subname = subname
        self._gov_point = gov_point
        self._exam_point = exam_point
        self._total_point = total_point

    def _subname_pat_check(self, subname):
        res = re.fullmatch(Subject._subname_pat, subname) and not re.search(Student._d_pat, subname)
        if not res:
            raise Exception

    def _check(self, gov_point, exam_point, total_point):
        """This function checks accordance of exam, government and total points."""
        self._check_gov(gov_point)
        self._check_exam(exam_point)
        self._check_total(total_point)
        self._check_total_exam(total_point, exam_point)
        self._check_total_gov(total_point, gov_point)

    def _check_gov(self, gov_point):
        if -1 <= gov_point <= 5 and gov_point != 1:
            pass
        else:
            raise Exception

    def _check_exam(self, exam_point):
        if 24 <= exam_point <= 40 or exam_point == 0:
            pass
        else:
            raise Exception

    def _check_total(self, total_point):
        if 0 <= total_point <= 100:
            pass
        else:
            raise Exception

    def _check_total_exam(self, total_point, exam_point):
        if total_point-exam_point <= 60:
            pass
        else:
            raise Exception

    def _check_total_gov(self, total_point, gov_point):
        if total_point >= 90 and gov_point == 5:
            pass
        elif 75 <= total_point < 90 and gov_point == 4:
            pass
        elif 60 <= total_point < 75 and gov_point == 3:
            pass
        elif 0 <= total_point < 60 and gov_point < 3:
            pass
        else: raise Exception

    @property
    def subname(self):
        return self._subname

    @property
    def total_point(self):
        return self._total_point

    def __lt__(self, other):
        if isinstance(other, int):
            return self.total_point < other
        elif isinstance(other, Subject):
            return self.total_point < other.total_point
        else:
            raise Exception

    def __le__(self, other):
        if isinstance(other, int):
            return self.total_point <= other
        elif isinstance(other, Subject):
            return self.total_point <= other.total_point
        else:
            raise Exception

    def __eq__(self, other):
        if isinstance(other, int):
            return self.total_point == other
        elif isinstance(other, Subject):
            return self.total_point == other.total_point
        else:
            raise Exception
