#Oleksii Frankov, K-13.1, V-116.

import json
import csv
from builder import *
from information import *

def load(info, csv_name, json_name, encoding_in):
    """This function loads input data."""
    print(f"input-csv {csv_name}:", end=" ")
    load_data(info, csv_name, encoding_in)
    print("OK")
    print(f"input-json {json_name}:", end=" ")
    num, stud_tails = load_stat(json_name, encoding_in)
    print("OK")
    print("json?=csv:", end=" ")
    if fit(info, num, stud_tails):
        print("OK")
    else:
        print("UPS")


def load_data(info, csv_name, encoding_in):
    """This function loads csv file."""
    info.clear()
    builder = Builder()
    builder.load(csv_name, encoding_in, info)
    

def load_stat(json_name, encoding_in):
    """This function loads json extra file."""
    with open(json_name, encoding=encoding_in) as f:
        data_extra = json.load(f)
    _load_stat_check(data_extra)
    return data_extra["загальна кількість записів"], data_extra["кількість студентів, у яких є «хвости»"]

def _load_stat_check(data_extra):
    if "загальна кількість записів" and "кількість студентів, у яких є «хвости»" in (data_extra.keys()):
        pass
    else:
        raise Exception

def fit(info, num, stud_tails):
    """This function checks if data of csv and json files fits each other."""
    res = info.num_records == num and info.tails == stud_tails
    return res
