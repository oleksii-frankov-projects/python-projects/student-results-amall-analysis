#Oleksii Frankov, K-13.1, V-116.

from information import *
import csv

class Builder():
    """This class uploads data in object of class Information from csv-file."""
    def __init__(self):
        self._gov_point = None
        self._name = None
        self._patronymic = None
        self._exam_point = None
        self._surname = None
        self._total_point = None
        self._subname = None
        self._ngroup = None
        self._npass = None
        self._line = None

    def load(self, csv_name, encoding_in, info):
        self._clear()
        """This method loads data from csv-file to object of class Information."""
        newline = None
        with open(csv_name, encoding=encoding_in, newline=newline) as f:
            r = csv.reader(f, delimiter=";")
            for self._line in r:
                if self._line: self._process(info)


    def _process(self, info):
        if not self._empty_line_check():
            self._len_check()
            self._white_border_check()
            self._convert_line()
            info.load(self._gov_point, self._name,
                  self._patronymic, self._exam_point,
                  self._surname, self._total_point,
                  self._subname, self._ngroup, self._npass)

    def _convert_line(self):
        """This method  converts and distributes values of line among variables of object of class Builder."""
        self._gov_point = self._int_convert(self._line[0])
        self._name = self._line[1]
        self._patronymic = self._line[2]
        self._exam_point = self._int_convert(self._line[3])
        self._surname = self._line[4]
        self._total_point = self._int_convert(self._line[5])
        self._subname = self._line[6]
        self._ngroup = self._line[7]
        self._npass = self._line[8]

    @staticmethod
    def _int_convert(value: str):
        return int(value)

    def _len_check(self):
        if len(self._line) != 9:
            raise Exception

    def _empty_line_check(self):
        return len(self._line)==1 and self._line[0].isspace()

    def _white_border_check(self):
        for el in self._line:
            if not el == el.strip():
                raise Exception

    def _clear(self):
        """This method deletes whole data in object of Builder."""
        self._gov_point = None
        self._name = None
        self._patronymic = None
        self._exam_point = None
        self._surname = None
        self._total_point = None
        self._subname = None
        self._ngroup = None
        self._npass = None
